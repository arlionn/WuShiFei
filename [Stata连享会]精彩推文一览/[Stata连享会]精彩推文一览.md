
> 作者：连玉君 ( [知乎](https://zhuanlan.zhihu.com/arlion) | [简书](http://www.jianshu.com/u/69a30474ef33) | [码云](https://gitee.com/arlionn) | [github](http://github.com/StataChina) )   

- stata应用篇
- 计量方法专题篇
- 神兵利器篇
- 讲故事篇
- 资源分享篇
- 独家心得篇
- 文字排版篇
- 其他  

## Stata应用篇
- [Stata: 你还在用reshape转换长宽数据吗？那你就OUT了！](https://www.jianshu.com/p/a5607b37fb78)

- [Stata小白系列之一：调入数据](https://www.jianshu.com/p/273ab0a9f3bb)

- [gen 和 egen 中的 sum() 函数](https://www.jianshu.com/p/07e47dac8bd2)

- [Stata：用 bytwoway 实现快速分组绘图](https://www.jianshu.com/p/1471cf87f25f)

- [怎么在Stata图形中附加水平线或竖直线？](https://www.jianshu.com/p/0f962953593a)

- [Stata15 Unicode：一次性转码，解决中文乱码问题](https://www.jianshu.com/p/4347685cc11e)

- [Stata 15 dofile 和 .dta 文件转码方法](https://www.jianshu.com/p/149a0f95deaa)

- [Stata15某些版本无法自动生成log文件问题](https://www.jianshu.com/p/8a0353c961cf)

- [Stata中的数据显示格式和四舍五入](https://www.jianshu.com/p/99b93ff7114c)

- [君生我未生！Stata - 论文四表一键出](https://www.jianshu.com/p/97c4f291ee1e)

- [Stata: 用esttab生成带组别名称的 LaTeX 回归表格](https://www.jianshu.com/p/0d327ec1f204)

- [输出相关系数矩阵至 Word / Excel 文档中：pwcorr_a 命令简介](https://www.jianshu.com/p/5fdce59244dd)

- [Stata小程序: 提取简书文章列表](https://www.jianshu.com/p/76f18c9b96ad)

- [Stata小抄：一组图记住Stata常用命令](https://www.jianshu.com/p/6b1e387f172b)

- [Stata: 如何检验分组回归后的组间系数差异？](https://www.jianshu.com/p/38315707ef6c)

- [Stata快捷键GIF：键盘就是你的武器](https://www.jianshu.com/p/813568a223e2)

- [如何处理时间序列中的日期间隔 (with gaps) 问题？](https://www.jianshu.com/p/1c8423ee6c13)

- [使用 -import fred- 命令导入联邦储备经济数据库 (FRED)](https://www.jianshu.com/p/87e4dae27864)

- [[转]Unicode 和 UTF-8 有何区别？](https://www.jianshu.com/p/ab72465608d8)

- [Stata 资料速查wiki(更新中)](https://gitee.com/arlionn/stata/wikis/Home)

## 计量方法专题篇
- [Stata： 双重差分的固定效应模型 (DID)](https://www.jianshu.com/p/e97c1dc05c2c)

- [离群值！离群值？离群值！](https://www.jianshu.com/p/0c967a1526ef)

- [盈余管理、过度投资怎么算？分组回归获取残差](https://www.jianshu.com/p/73bc73a87d6c)

- [协整：醉汉牵着一条狗](https://www.jianshu.com/p/08e194e14b7a)

- [Fuzzy Differences-in-Differences （模糊倍分法）](https://www.jianshu.com/p/8918037d76a1)

- [DSGE 模型的 Stata 实现](https://www.jianshu.com/p/4579812bd0f7)


## 神兵利器篇
- [最近看到的几个神器小软件](https://www.jianshu.com/p/6be12a7e4d35)

- [可汗学院风格电子板书攻略: Wacom+ArtRage](https://www.jianshu.com/p/717b2e689d96)

- [EndNote X7，X8 使用说明](https://www.jianshu.com/p/af1c15c079a3)

- [码云：我把常用小软件都放这儿了](https://www.jianshu.com/p/fbb6bdeb9df5)

- [一个博士生该掌握哪些基本工具(武器)？](https://www.jianshu.com/p/90d6a54e35a5)

- [Stata+搜狗=？=效率！（搜狗自定义短语）](https://www.jianshu.com/p/4c27c66757a8)


## 讲故事篇
- [Stata可视化：让他看懂我的结果！](https://www.jianshu.com/p/43fe2339c90c)

- [用 Stata 制作教学演示动态图 GIF](https://www.jianshu.com/p/d9f146c690f7)

- [Stata dofile 转换 PDF 制作讲义方法](https://www.jianshu.com/p/b119033d8b93)

- [Stata兄弟：提问时能否有点诚意？用 dataex 吧](https://www.jianshu.com/p/9870080fe769)

- [彼此不再煎熬-如何做好毕业答辩陈述？](https://www.jianshu.com/p/086f9cf2cc30)


## 资源分享篇
- [Cameron 教授提供的 Stata 资源](https://www.jianshu.com/p/b679403c795a)

- [哇！Stata 书库来袭！](https://www.jianshu.com/p/f1c4b8762709)

- [Stata帮助和网络资源汇总(持续更新中) ](https://www.jianshu.com/p/c723bb0dbf98)

- [Github使用方法及Stata资源](https://www.jianshu.com/p/d2ee76b0f74c)

- [第一届stata中国用户大会嘉宾PDF讲稿下载](https://www.jianshu.com/p/db46fae3fd95)


## 独家心得篇
- [连玉君的链接](
https://www.jianshu.com/p/494e6feab565)

- [连玉君 Markdown 笔记](
https://www.jianshu.com/p/db1d26af109d)

- [Stata：Mata 笔记](
https://www.jianshu.com/p/03d138ff81da)

- [高考那年，擦肩而过的军校](
https://www.jianshu.com/p/fe79b4aa8b48)

- [米小圈启示：让孩子尽情搞怪](
https://www.jianshu.com/p/c17d28dc68f8)


## 文字排版篇
- [Mathtype与LaTeX公式](
https://www.jianshu.com/p/51dd3fff53b2)

- [一键将 Word 转换为 Markdown](
https://www.jianshu.com/p/df6a136d06d8)

- [在 Markdown 中使用 HTML 特殊符号](
https://www.jianshu.com/p/e65e1e36056b)

- [在 Markdown 中快速插入文字连接](
https://www.jianshu.com/p/ff3b1fa07a97)


## 其他
- [数据分析修炼历程：你在哪一站？](
https://www.jianshu.com/p/7382f9f57605)


